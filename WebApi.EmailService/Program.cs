﻿using Autofac;
using NServiceBus;
using System;
using System.Net;
using WebApi.EmailService.Email;

namespace WebApi.EmailService
{
    class Program
    {
        private static EmailSender _mailSender { get; set; }

        static void Main(string[] args)
        {
            // todo
            var mailSettings = new EmailSettings
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                SmtpDeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("woonderluck@gmail.com", "")
            };
            var emailSender = new EmailSender(mailSettings);

            Console.Title = "EmailService.Server";
            var busConfiguration = new BusConfiguration();
            busConfiguration.EndpointName("EmailService.Server");
            busConfiguration.UseSerialization<JsonSerializer>();
            busConfiguration.EnableInstallers();
            busConfiguration.UsePersistence<InMemoryPersistence>();

            using (var bus = Bus.Create(busConfiguration).Start())
            {
                Console.WriteLine("Listening...   Press any key to exit");
                Console.ReadKey();
            }
        }

        private void RegisterDependencies(ref ContainerBuilder builder)
        {

        }
    }
}
