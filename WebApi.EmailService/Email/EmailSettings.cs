﻿using System.Net;
using System.Net.Mail;

namespace WebApi.EmailService.Email
{
    public class EmailSettings
    {
        public string Host { get; set; }

        public int Port { get; set; }

        public bool EnableSsl { get; set; }

        public SmtpDeliveryMethod SmtpDeliveryMethod { get; set; }

        public bool UseDefaultCredentials { get; set; }

        public NetworkCredential Credentials { get; set; }
    }
}
