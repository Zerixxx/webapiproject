﻿using WebApi.EmailService.Models;

namespace WebApi.EmailService.Email
{
    public interface IEmailSender
    {
        void Send(EmailMessage message);
    }
}
