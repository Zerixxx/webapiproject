﻿using System;
using System.Net.Mail;
using WebApi.EmailService.Models;

namespace WebApi.EmailService.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly SmtpClient _mailClinet;

        public EmailSender(EmailSettings settings)
        {
            _mailClinet = new SmtpClient()
            {
                Host = settings.Host,
                Port = settings.Port,
                EnableSsl = settings.EnableSsl,
                DeliveryMethod = settings.SmtpDeliveryMethod,
                UseDefaultCredentials = settings.UseDefaultCredentials,
                Credentials = settings.Credentials
            };
        }

        public void Send(EmailMessage message)
        {
            try
            {
                using(var _message = new MailMessage(message.From, message.To, message.Subject, message.Body))
                {
                    _mailClinet.Send(_message);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /*
        public async Task SendAsync(EmailMessage message)
        {
            try
            {
                using (var _message = new MailMessage(message.From, message.To, message.Subject, message.Body))
                {
                    await _mailClinet.SendMailAsync(_message);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        */
    }
}
