﻿using NServiceBus;
using System;
using WebApi.Entity.Commands;

namespace WebApi.EmailService
{
    public class TicketOrderCommandHandler : IHandleMessages<TicketOrderCommand>
    {
        public void Handle(TicketOrderCommand command)
        {
            Console.WriteLine($"TicketOrderCommandHandler(): command with id={command.Id} is handled.");
        }
    }
}
