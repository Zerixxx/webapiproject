﻿using System;
using System.Collections.Generic;
using WebApi.DAL.Abstract;
using WebApi.Entity;
using WebApi.Service.Abstract;

namespace WebApi.Service
{
    public class UserService : IUserService
    {
        private IUserRepository _repository;
        public IUserRepository Repository
        {
            get
            {
                return _repository;
            }

            set
            {
                _repository = value;
            }
        }

        public void Create(User user)
        {
            try
            {
                Repository.Create(user);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<User> GetAll()
        {
            return Repository.GetAll();
        }

        public User GetUserById(int id)
        {
            return Repository.GetById(id);
        }

        public bool isEntityValid(User entity)
        {
            return !string.IsNullOrEmpty(entity.FirstName); 
        }


    }
}
