﻿using System;
using System.Collections.Generic;
using WebApi.Entity;

namespace WebApi.Service.Abstract
{
    public interface IDbContext<T> where T: IEntity
    {
        bool Create(T entity);
        bool Update(T entity);
        bool Delete(T entity);

        T GetById(Guid id);
        IEnumerable<T> GetAll();
    }
}
