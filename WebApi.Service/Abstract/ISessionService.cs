﻿using System;
using WebApi.DAL.Abstract;
using WebApi.Entity;

namespace WebApi.Service.Abstract
{
    public interface ISessionService : IValidationService<Session>
    {
        ISessionRepository Repository { get; set; }

        void CreateSession(Session session);
    }
}
