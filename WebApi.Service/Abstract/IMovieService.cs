﻿using System.Collections.Generic;
using WebApi.DAL.Abstract;
using WebApi.Entity;

namespace WebApi.Service.Abstract
{
    public interface IMovieService: IValidationService<Movie>
    {
        IMovieRepository Repository { get; set; }

        void CreateMovie(Movie model);

        IEnumerable<Movie> FindMoviesByName(string name);

        string TestGet();
    }
}
