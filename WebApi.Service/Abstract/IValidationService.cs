﻿using WebApi.Entity;

namespace WebApi.Service.Abstract
{
    public interface IValidationService<T> where T: IEntity
    {
        bool isEntityValid(T entity);
    }
}
