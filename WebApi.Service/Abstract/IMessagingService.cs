﻿
namespace WebApi.Service.Abstract
{
    public interface IMessagingService
    {
        void SendCommand<T>(T command);
        void SendCommand<T>(string endpointAddress, T command);
    }
}
