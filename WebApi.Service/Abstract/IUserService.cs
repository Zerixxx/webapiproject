﻿using System.Collections.Generic;
using WebApi.DAL.Abstract;
using WebApi.Entity;

namespace WebApi.Service.Abstract
{
    public interface IUserService: IValidationService<User>
    {
        IUserRepository Repository { get; set; }

        User GetUserById(int id);

        void Create(User user);

        IEnumerable<User> GetAll(); 
    }
}
