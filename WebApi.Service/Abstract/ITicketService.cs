﻿using WebApi.DAL.Abstract;
using WebApi.Entity;
using WebApi.Entity.Commands;

namespace WebApi.Service.Abstract
{
    public interface ITicketService : IValidationService<Ticket>
    {
        ITicketRepository Repository { get; set; }
        Ticket OrderTicket(int userId, Session session);
        void OrderTicket(Ticket ticket);
    }
}
