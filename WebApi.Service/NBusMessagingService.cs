﻿using NServiceBus;
using WebApi.Service.Abstract;

namespace WebApi.Service
{
    public class NBusMessagingService : IMessagingService
    {
        private readonly BusConfiguration _config;

        public NBusMessagingService(BusConfiguration config)
        {
            _config = config;
        }

        public void SendCommand<T>(T command)
        {
            using (var bus = Bus.Create(_config).Start())
            {
                bus.Send(command);
            }
        }

        public void SendCommand<T>(string endpointAddress, T command)
        {
            using (var bus = Bus.Create(_config).Start())
            {
                bus.Send(endpointAddress, command);
            }
        }
    }
}
