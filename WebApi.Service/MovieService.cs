﻿using System;
using System.Collections.Generic;
using WebApi.DAL.Abstract;
using WebApi.Entity;
using WebApi.Service.Abstract;
using WebApi.Service.Exceptions;

namespace WebApi.Service
{
    public class MovieService : IMovieService
    {
        private IMovieRepository _repository;

        public IMovieRepository Repository
        {
            get
            {
                return _repository;
            }

            set
            {
                _repository = value;
            }
        }

        public void CreateMovie(Movie model)
        {
            try
            {
                if (isEntityValid(model))
                {
                    Repository.Create(model);
                }
                else
                {
                    throw new InvalidModelException();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Movie> FindMoviesByName(string name)
        {
            try
            {
                return Repository.FindByName(name);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool isEntityValid(Movie entity)
        {
            return entity is Movie && 
                !string.IsNullOrEmpty(entity.Name) && 
                entity.ReleaseDate != null;
        }

        public string TestGet()
        {
            return "Movie Service Test String";
        }
    }
}
