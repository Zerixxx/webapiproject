﻿using System;
using WebApi.DAL.Abstract;
using WebApi.Entity;
using WebApi.Service.Abstract;

namespace WebApi.Service
{
    public class SessionService : ISessionService
    {
        private ISessionRepository _repository;
        private readonly IMovieService _movieService;

        public SessionService(IMovieService movieService)
        {
            _movieService = movieService;
        }

        public ISessionRepository Repository
        {
            get
            {
                return _repository;
            }

            set
            {
                _repository = value;
            }
        }

        public bool isEntityValid(Session entity)
        {
            bool isValid =
                _movieService.isEntityValid(entity.Movie)
                && entity.Price > 0
                && entity.StartTime >= DateTime.Now;

            return isValid;
        }

        public void CreateSession(Session session)
        {
            Repository.Create(session);
        }
    }
}
