﻿using System;
using WebApi.DAL.Abstract;
using WebApi.Entity;
using WebApi.Entity.Commands;
using WebApi.Service.Abstract;
using WebApi.Service.Exceptions;

namespace WebApi.Service
{
    public class TicketService : ITicketService
    {
        private ITicketRepository _repository;
        private readonly IMovieService _movieService;
        private readonly IUserService _userService;
        private readonly ISessionService _sessionService;
        private readonly Lazy<IMessagingService> _messaginService;

        // todo: add reading from app.config 
        private const string _emailServiceEndpointName = "EmailService.Server";
        
        public ITicketRepository Repository
        {
            get
            {
                return _repository;
            }

            set
            {
                _repository = value;
            }
        }

        // todo: move all db services into UnitOfWork
        public TicketService(IMovieService movieService, 
            IUserService userService,
            ISessionService sessionService,
            Lazy<IMessagingService> messagingService)
        {
            _movieService = movieService;
            _userService = userService;
            _sessionService = sessionService;
            _messaginService = messagingService;
        }

        public Ticket OrderTicket(int userId, Session session)
        {
            var user = _userService.GetUserById(userId);
            var ticket = new Ticket { Customer = user, Session = session };
            OrderTicket(ticket);
            return ticket;
        }

        public bool isEntityValid(Ticket entity)
        {
            bool isValid = _userService.isEntityValid(entity.Customer)
                && _sessionService.isEntityValid(entity.Session);

            return isValid;

        }

        public void OrderTicket(Ticket ticket)
        {
            try
            {
                if (!isEntityValid(ticket))
                {
                    throw new InvalidModelException();
                }

                Repository.Create(ticket);
                _messaginService.Value.SendCommand(_emailServiceEndpointName, new TicketOrderCommand
                {
                    Id = Guid.NewGuid(),
                    ticket = ticket
                });
                
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
