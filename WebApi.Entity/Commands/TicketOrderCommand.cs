﻿using NServiceBus;
using System;

namespace WebApi.Entity.Commands
{
    public class TicketOrderCommand : ICommand
    {
        public Guid Id { get; set; }
        public Ticket ticket { get; set; }
    }
}
