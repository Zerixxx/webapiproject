﻿using System.Net.Mail;

namespace WebApi.EmailService.Models
{
    public class EmailMessage
    {
        public int Id { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}
