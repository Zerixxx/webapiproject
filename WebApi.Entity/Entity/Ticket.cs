﻿namespace WebApi.Entity
{
    public class Ticket : IEntity
    {
        public virtual int Id { get; set; }

        public virtual User Customer { get; set; }

        public virtual Session Session { get; set; }
    }
}
