﻿using System;

namespace WebApi.Entity
{
    public class Movie : IEntity
    {
        public virtual int Id { get; set; }
       
        public virtual string Name { get; set; }
         
        public virtual DateTime ReleaseDate { get; set; }
    }
}
