﻿using System;

namespace WebApi.Entity
{
    public class Session : IEntity
    {
        public virtual int Id { get; set; }

        public virtual Movie Movie { get; set; }

        public virtual decimal Price { get; set; }

        public virtual DateTime StartTime { get; set; }
    }
}
