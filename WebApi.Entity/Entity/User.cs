﻿using System;

namespace WebApi.Entity
{
    public class User : IEntity
    {
        public virtual int Id { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string Email { get; set; }
    }
}
