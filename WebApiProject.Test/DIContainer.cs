﻿using Autofac;
using WebApi.Service;
using WebApi.Service.Abstract;

namespace WebApi.Test
{
    public class DIContainer
    {
        // TODO: revert this
        public static IContainer Resolver { get; set; }

        static DIContainer()
        {
            var builder = new ContainerBuilder();
            RegisterTypes(ref builder);
            Resolver = builder.Build();
        }

        static private void RegisterTypes(ref ContainerBuilder builder)
        {
            builder.RegisterType<MovieService>().As<IMovieService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<SessionService>().As<ISessionService>();
            builder.RegisterType<TicketService>().As<ITicketService>();
        }
    }
}
