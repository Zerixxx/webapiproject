﻿using Autofac;
using Moq;
using NUnit.Framework;
using System;
using WebApi.DAL.Abstract;
using WebApi.Entity;
using WebApi.Service;
using WebApi.Service.Abstract;
using WebApi.Service.Exceptions;

namespace WebApi.Test
{
    [TestFixture]
    class TicketServiceTest
    {
        private IContainer _resolver = DIContainer.Resolver;

        private Mock<IUserService> _userServiceMock = new Mock<IUserService>();
        private Mock<IMovieService> _movieServiceMock = new Mock<IMovieService>();
        private Mock<ISessionService> _sessionServiceMock = new Mock<ISessionService>();
        private Mock<IMessagingService> _messagingServiceMock = new Mock<IMessagingService>();
        
        private Mock<ITicketRepository> _repositoryMock = new Mock<ITicketRepository>();


        [Test]
        public void TicketShouldBeOrderedSuccessfully()
        {
            //arrange
            var userId = 0;
            var session = TestData.SessionEntity;
            _userServiceMock
                .Setup(m => m.GetUserById(It.IsAny<int>()))
                .Returns(TestData.UserEntity);

            _userServiceMock
                .Setup(m => m.isEntityValid(It.IsAny<User>()))
                .Returns(true);

            _sessionServiceMock
                .Setup(m => m.isEntityValid(It.IsAny<Session>()))
                .Returns(true);

            var lazyMessaginService = new Lazy<IMessagingService>(() => _messagingServiceMock.Object);
            var service = new TicketService(_movieServiceMock.Object, _userServiceMock.Object, _sessionServiceMock.Object, lazyMessaginService);
            service.Repository = _repositoryMock.Object;

            //act
            Ticket ticket = service.OrderTicket(userId, session);

            //assert
            Assert.That(ticket is Ticket);
        }

        [Test]
        public void TicketOrderingShouldFailIfTicketModelIsIncorrect()
        {
            //arrange
            var userId = 0;
            Session session = null;

            _userServiceMock
                .Setup(m => m.isEntityValid(It.IsAny<User>()))
                .Returns(true);

            _sessionServiceMock
                .Setup(m => m.isEntityValid(It.IsAny<Session>()))
                .Returns(false);

            var lazyMessaginService = new Lazy<IMessagingService>(() => _messagingServiceMock.Object);
            var service = new TicketService(_movieServiceMock.Object, _userServiceMock.Object, _sessionServiceMock.Object, lazyMessaginService);

            //assert
            Assert.That(() => service.OrderTicket(userId, session), Throws.TypeOf<InvalidModelException>());
        }

        [Test]
        public void TicketModelShouldBeValid()
        {
            //arrange
            var model = TestData.TicketEntity;
            _userServiceMock
                .Setup(m => m.isEntityValid(It.IsAny<User>()))
                .Returns(true);

            _sessionServiceMock
                .Setup(m => m.isEntityValid(It.IsAny<Session>()))
                .Returns(true);

            var lazyMessaginService = new Lazy<IMessagingService>(() => _messagingServiceMock.Object);
            var service = new TicketService(_movieServiceMock.Object, _userServiceMock.Object, _sessionServiceMock.Object, lazyMessaginService);

            // act
            bool isValid = service.isEntityValid(model);

            //assert
            Assert.IsTrue(isValid);
        }

        [Test]
        public void TicketModelWithPastSessionShouldBeInvalid()
        {
            //arrange
            var model = TestData.TicketEntity;
            model.Session.StartTime = DateTime.Now.AddDays(-1);

            _userServiceMock
                .Setup(m => m.isEntityValid(It.IsAny<User>()))
                .Returns(true);

            _sessionServiceMock
                .Setup(m => m.isEntityValid(It.IsAny<Session>()))
                .Returns(false);

            var lazyMessaginService = new Lazy<IMessagingService>(() => _messagingServiceMock.Object);
            var service = new TicketService(_movieServiceMock.Object, _userServiceMock.Object, _sessionServiceMock.Object, lazyMessaginService);

            // act
            bool isValid = service.isEntityValid(model);

            //assert
            Assert.IsFalse(isValid);
        }
    }
}
