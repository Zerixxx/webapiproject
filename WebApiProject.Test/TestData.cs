﻿using System;
using WebApi.Entity;

namespace WebApi.Test
{
    public class TestData
    {
        public static User UserEntity = new User { Id = 0, FirstName = "First", LastName = "Last", Email = "email@mail.com" };
        public static Movie MovieEntity = new Movie { Id = 0, Name = "Name", ReleaseDate = DateTime.Now.AddDays(1) };
        public static Session SessionEntity = new Session { Id = 0, Movie = MovieEntity, Price = 5, StartTime = DateTime.Now.AddDays(1) };
        public static Ticket TicketEntity = new Ticket { Id = 0, Customer = UserEntity, Session = SessionEntity };
    }
}