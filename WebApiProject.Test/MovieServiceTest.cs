﻿using NUnit.Framework;
using Moq;
using WebApi.Entity;
using System.Collections.Generic;
using System.Linq;
using WebApi.DAL.Abstract;
using Autofac;
using WebApi.Service.Exceptions;
using WebApi.Service.Abstract;

namespace WebApi.Test
{
    [TestFixture]
    public class MovieServiceTest
    {
        private Mock<IMovieRepository> _repositoryMock = new Mock<IMovieRepository>();
        private IContainer _resolver = DIContainer.Resolver;
        private IMovieService _movieService;

        [SetUp]
        public void SetUp()
        {
            _movieService = _resolver.Resolve<IMovieService>();
        }

        [Test]
        public void CreateMovieShouldBeSuccessful()
        {
            //arrange
            var movie = TestData.MovieEntity;
            _movieService.Repository = _repositoryMock.Object;

            //assert
            Assert.DoesNotThrow(() => _movieService.CreateMovie(movie));
        }

        [Test]
        public void CreateNullMovieShouldThrowException()
        {
            //arrange
            Movie movie = null;
            _movieService.Repository = _repositoryMock.Object;

            // assert
            Assert.That(() => _movieService.CreateMovie(movie), Throws.TypeOf<InvalidModelException>());
        }

        [Test]
        public void FindMoviesByNameShouldReturnNotNullResult()
        {
            //arrange
            string movieName = "Movie1";
            _repositoryMock.Setup(m => m.FindByName(It.IsAny<string>())).Returns(new List<Movie>() {
                new Movie()
                {
                    Id = 0,
                    Name = movieName
                },
                new Movie()
                {
                    Id = 1,
                    Name = "Movie2"
                },
                new Movie()
                {
                    Id = 2,
                    Name = "Movie3"
                },
            });

            _movieService.Repository = _repositoryMock.Object;

            //act
            var movies = _movieService.FindMoviesByName(movieName);

            // assert
            Assert.That(movies is IEnumerable<Movie>);
            Assert.That(movies.Any());
        }

        [Test]
        public void FindMoviesByNameShouldReturnEmptyCollection()
        {
            //arrange
            string movieName = "Movie1";
            _repositoryMock.Setup(m => m.GetAll()).Returns(new List<Movie>());
            _movieService.Repository = _repositoryMock.Object;

            //act
            var movies = _movieService.FindMoviesByName(movieName);

            // assert
            Assert.That(movies is IEnumerable<Movie>);
            Assert.That(!movies.Any());
        }

        [Test]
        public void IsEntityValidShouldReturnTrue()
        {
            //arrange
            Movie movie = TestData.MovieEntity;

            //act
            bool isEntityValid = _movieService.isEntityValid(movie);

            // assert
            Assert.IsTrue(isEntityValid);
        }

        [Test]
        public void IsEntityValidShouldReturnFalse()
        {
            //arrange
            Movie movie = new Movie();

            //act
            bool isEntityValid = _movieService.isEntityValid(movie);

            // assert
            Assert.IsFalse(isEntityValid);
        }
    }
}
