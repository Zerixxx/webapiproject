﻿using Autofac;
using System;
using System.Collections.Generic;

namespace TestApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            /*
            var builder = new ContainerBuilder();
            builder.RegisterType<BooType>().As<IBooType>();
            builder.RegisterType<FooType>().PropertiesAutowired().As<IFooType>().InstancePerDependency();

            DIContainer.Container = builder.Build();

            Test();
            */

            Console.WriteLine();
            Console.ReadKey();
        }

        static void Test()
        {
            IFooType foo = DIContainer.Container.Resolve<IFooType>(); 
            IFooType foo1 = DIContainer.Container.Resolve<IFooType>();


            Console.WriteLine(foo.Equals(foo1));
            
        }
    }

    public class FooType : IFooType, IDisposable
    {
        public IBooType Prop { get; set; }

        private bool isDisposed = false;

        public void Dispose()
        { 
            Console.WriteLine("Foo is Disposed");
        }

        public void Foo()
        {
            if (!isDisposed) isDisposed = true;
            else
            {
                throw new Exception("foo is disposed");
            }

            Console.WriteLine("Foo is called");
        }
    }

    public interface IFooType
    {
        void Foo();
        IBooType Prop { get; set; }
    }

    public class BooType : IBooType
    {
        public void Boo()
        {
            Console.WriteLine("Boo is called");
        }
    }

    public interface IBooType
    {
        void Boo();
    }
}
