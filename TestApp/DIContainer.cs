﻿using Autofac;

namespace TestApp
{
    public static class DIContainer
    {
        public static IContainer Container { get; set; }
    }
}
