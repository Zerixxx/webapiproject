﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Entity;
using WebApi.Service.Abstract;

namespace WebApiProject.Controllers
{
    [Route("api/ticket")]
    public class TicketController : Controller
    {
        private readonly ITicketService _ticketService;

        public TicketController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpPost]
        public void Post([FromBody]Ticket ticket)
        {
            _ticketService.OrderTicket(ticket);
        }
    }
}
