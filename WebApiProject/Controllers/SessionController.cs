﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Entity;
using WebApi.Service.Abstract;

namespace WebApiProject.Controllers
{
    [Route("api/session")]
    public class SessionController : Controller
    {
        private readonly ISessionService _sessionService;

        public SessionController(ISessionService sessionService)
        {
            _sessionService = sessionService;
        }

        [HttpPost]
        public void Post([FromBody]Session session)
        {
            _sessionService.CreateSession(session);
        }
    }
}
