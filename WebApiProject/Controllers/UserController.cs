﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebApi.Entity;
using WebApi.Service.Abstract;

namespace WebApiProject.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public List<User> GetAll()
        {
            var users = _userService.GetAll().ToList();
            return users;
        }

        [HttpPost]
        public void Post([FromBody] User user)
        {
            _userService.Create(user);
        }
    }
}
