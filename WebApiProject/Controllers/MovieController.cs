﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebApi.Service.Abstract;
using WebApi.Entity;

namespace WebApiProject.Controllers
{
    [Route("api/movie")]
    public class MovieController : Controller
    {

        public readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", _movieService.TestGet() };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Movie movie)
        {
            _movieService.CreateMovie(movie);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
