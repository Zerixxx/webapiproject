﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using WebApi.Service;
using WebApi.Service.Abstract;
using NServiceBus;
using WebApi.DAL.Abstract;
using WebApi.DAL.Repository;

namespace WebApiProject
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IContainer ApplicationContainer { get; private set; }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddMvc();

            // Create the container builder.
            var builder = new ContainerBuilder();

            // Register dependencies, populate the services from
            // the collection, and build the container. If you want
            // to dispose of the container at the end of the app,
            // be sure to keep a reference to it as a property or field.
            RegisterDependecies(ref builder);
            builder.Populate(services);
            this.ApplicationContainer = builder.Build();

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime appLifetime)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseMvc();

            // If you want to dispose of resources that have been resolved in the
            // application container, register for the "ApplicationStopped" event.
            appLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());
        }

        private void RegisterDependecies(ref ContainerBuilder builder)
        {
            // NServiceBus Client Setup
            var busConfiguration = new BusConfiguration();
            busConfiguration.EndpointName("EmailService.Client");
            busConfiguration.UseSerialization<JsonSerializer>();
            busConfiguration.EnableInstallers();
            busConfiguration.UsePersistence<InMemoryPersistence>();
            var messagingService = new NBusMessagingService(busConfiguration);

            //
            builder.RegisterType<MovieRepository>().As<IMovieRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<SessionRepository>().As<ISessionRepository>();
            builder.RegisterType<TicketRepository>().As<ITicketRepository>();

            builder.RegisterType<MovieService>().PropertiesAutowired().As<IMovieService>().InstancePerLifetimeScope();
            builder.RegisterType<UserService>().PropertiesAutowired().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<SessionService>().PropertiesAutowired().As<ISessionService>().InstancePerLifetimeScope();
            builder.RegisterType<TicketService>().PropertiesAutowired().As<ITicketService>().InstancePerLifetimeScope();

            builder.RegisterInstance(messagingService).As<IMessagingService>().SingleInstance();
        }
    }
}
