﻿using FluentNHibernate.Mapping;
using WebApi.Entity;

namespace WebApi.DAL.Mappings
{
    public class MovieMap : ClassMap<Movie>
    {
        public MovieMap()
        {
            Id(m => m.Id);
            Map(m => m.Name);
            Map(m => m.ReleaseDate);

            Table("Movies");
        }
    }

    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(m => m.Id);
            Map(m => m.FirstName);
            Map(m => m.LastName);
            Map(m => m.Email);

            Table("Users");
        }
    }

    public class SessionMap : ClassMap<Session>
    {
        public SessionMap()
        {
            Id(m => m.Id);
            Map(m => m.Price);
            Map(m => m.StartTime);
            References(m => m.Movie).Column("MovieId");

            Table("Sessions");
        }
    }

    public class TicketMap : ClassMap<Ticket>
    {
        public TicketMap()
        {
            Id(m => m.Id);
            References(m => m.Session).Column("SessionId");
            References(m => m.Customer).Column("CustomerId");

            Table("Tickets");
        }
    }
}
