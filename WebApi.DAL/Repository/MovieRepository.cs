﻿using System.Collections.Generic;
using System.Linq;
using WebApi.DAL.Abstract;
using WebApi.Entity;

namespace WebApi.DAL.Repository
{
    public class MovieRepository : BaseRepository<Movie>, IMovieRepository
    {
        public IEnumerable<Movie> FindByName(string name)
        {
            return base.GetAll().Where(m => m.Name.Contains(name));
        }
    }
}
