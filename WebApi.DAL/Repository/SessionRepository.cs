﻿using WebApi.DAL.Abstract;
using WebApi.Entity;

namespace WebApi.DAL.Repository
{
    public class SessionRepository : BaseRepository<Session>, ISessionRepository
    {
    }
}
