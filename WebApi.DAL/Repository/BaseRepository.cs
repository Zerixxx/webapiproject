﻿using NHibernate;
using NHibernate.Linq;
using System.Collections.Generic;
using WebApi.DAL.Abstract;
using WebApi.Entity;

namespace WebApi.DAL.Repository
{
    /// <summary>
    /// NHibernate repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseRepository<T> : IRepository<T> where T: IEntity 
    {
        private readonly ISession _session;

        public BaseRepository()
        {
            _session = NHibernateHelper.OpenSession();
        }

        public void Create(T entity)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Save(entity);
                transaction.Commit();
            }
        }

        public void Delete(T entity)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Delete(entity);
                transaction.Commit();
            }
        }

        public IEnumerable<T> GetAll()
        {
            return _session.Query<T>();
        }

        public T GetById(int id)
        {
            return _session.Get<T>(id);
        }

        public void Update(T entity)
        {
            using (var transaction = _session.BeginTransaction())
            {
                _session.Update(entity);
                transaction.Commit();
            }
        }
    }
}
