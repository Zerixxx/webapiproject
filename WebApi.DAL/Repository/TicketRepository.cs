﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.DAL.Abstract;
using WebApi.Entity;

namespace WebApi.DAL.Repository
{
    public class TicketRepository : BaseRepository<Ticket>, ITicketRepository
    {
    }
}
