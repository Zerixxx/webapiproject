﻿using WebApi.DAL.Abstract;
using WebApi.Entity;

namespace WebApi.DAL.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
    }
}
