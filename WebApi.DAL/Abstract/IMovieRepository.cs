﻿using System.Collections.Generic;
using WebApi.Entity;

namespace WebApi.DAL.Abstract
{
    public interface IMovieRepository: IRepository<Movie>
    {
        IEnumerable<Movie> FindByName(string name);
    }
}
