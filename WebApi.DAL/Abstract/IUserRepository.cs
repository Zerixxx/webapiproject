﻿using WebApi.Entity;

namespace WebApi.DAL.Abstract
{
    public interface IUserRepository : IRepository<User>
    {

    }
}
