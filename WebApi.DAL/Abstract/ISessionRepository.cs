﻿using WebApi.Entity;

namespace WebApi.DAL.Abstract
{
    public interface ISessionRepository : IRepository<Session>
    {
    }
}
