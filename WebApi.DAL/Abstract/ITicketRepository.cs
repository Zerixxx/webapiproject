﻿using WebApi.Entity;

namespace WebApi.DAL.Abstract
{
    public interface ITicketRepository : IRepository<Ticket>
    {
    }
}
