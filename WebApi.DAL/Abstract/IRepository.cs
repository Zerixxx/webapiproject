﻿using System.Collections.Generic;
using WebApi.Entity;

namespace WebApi.DAL.Abstract
{
    public interface IRepository<T> where T : IEntity
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
