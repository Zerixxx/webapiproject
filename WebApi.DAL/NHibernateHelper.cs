﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using WebApi.DAL.Mappings;

namespace WebApi.DAL
{
    public class NHibernateHelper
    {
        private static ISessionFactory _sessionFactory;
        private static ISession _session;
        static readonly object sessionLock = new object();

        private const string _connectionString = @"Data Source=(local);Initial Catalog=TestMovieDatabase;Integrated Security=True";

        private static ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                    InitializeSessionFactory();
                return _sessionFactory;
            }
        }

        private static void InitializeSessionFactory()
        {
            _sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008
                  .ConnectionString(_connectionString)
                  .ShowSql()
                )
                .Mappings(m =>
                          m.FluentMappings
                              .AddFromAssemblyOf<MovieMap>())
                .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                .BuildSessionFactory();
        }

        private static bool IsOpenedSession
        {
            get { return _session != null && _session.IsOpen; }
        }

        public static ISession OpenSession()
        {
            lock (sessionLock)
            {
                if (!IsOpenedSession)
                {
                    _session = SessionFactory.OpenSession();
                }
            }
            return _session;
        }
    }
}
